# Chimpanzee tractography protocols

This repository contains the tractography protocols for the paper:

Bryant KL, Li L, Mars RB (2020) A comprehensive atlas of white matter tracts in the chimpanzee. bioRxiv. doi:[10.1101/2020.01.24.918516](https://www.biorxiv.org/content/10.1101/2020.01.24.918516v1).

This paper is currently under peer-review. Please contact [katherine.bryant@ndcn.ox.ac.uk](mailto:katherine.bryant@ndcn.ox.ac.uk) for more information.

This paper presents tractography recipes in standard space for the major white matter bundles of the chimpanzee brain and, for comparison, the human and macaque brain.

The directory `recipes` contains tractography recipes, according to the conventions of [FSL's Xtract tool](fsl.fmrib.ox.ac.uk/fsl/fslwiki/XTRACT) as described in Warrington S, Bryant KL, Khrapitchev AA, Sallet J, Charquero-Ballester M, Douaud G, Jbabdi S, Mars RB, Sotiropoulos SN (2020) XTRACT - Standardised protocols for automated tractography and connectivity blueprints in the human and macaque brain. NeuroImage 217:116923

Standard space is the 'Chimplate' Yerkes29 space. Mean `dyads1` and `mean_f1samples` for display are in the main directory. The human and macaque data are compatible with MNI and F99 space, respectively.

The `tract_volumes` directory contains average tracts for all bundles using the recipes. The `surface_projections` contains their surface projections created using the blueprint approach of [Mars et al.](https://elifesciences.org/articles/35237) (2018) Whole brain comparative anatomy using connectivity blueprints. eLife 7:e35237

Figures 4-11 and S6 in the manuscript were generated in the following matter: Probabilistic tractograms (figs. 4-11) were were visualized with Matlab (MATLAB and Statistics Toolbox Release 2012b, The MathWorks, Inc., Natick, Massachusetts, United States). Deterministic tractography (fig. S6) was run on individual chimpanzees using DSI Studio March 10, 2020 build (http://dsi-studio.labsolver.org).